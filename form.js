jQuery(document).ready(function() {
    "use strict";
    $('#contactform').submit(function() {

        var action = $(this).attr('action');

        $("#message").fadeOut(0, function() {
            $('#message').hide();

            $('#submit')
                .attr('disabled', 'disabled');
            var form = $("#contactform").serialize();
            $.post(action, form,
                function(data) {
                    document.getElementById('message').innerHTML = data;
                    $('#message').fadeIn(200);
                    $('.hide').hide(0);
                    $('#submit').removeAttr('disabled');
                    //				if(data.match('success') != null) $('#contactform').fadeOut('slow');

                }
            );

        });

        return false;

    });

});