<?php
if(!$_POST) exit;
$units = intval($_POST['wpcc_structure'][8]); 

if($units > 0 && $units <= 4) {
    $amount = 24000;
	exit('<span style="font-weight: bold; font-size: 18px;">'.$amount.' ₸ в месяц</span></div>');
} else if($units > 4 && $units <= 15) {
    $amount = $units * 5500;
	exit('<span style="font-weight: bold; font-size: 18px;">'.$amount.' ₸ в месяц</span></div>');
} else if($units > 15 && $units <= 25) {
    $amount = $units * 5000;
	exit('<span style="font-weight: bold; font-size: 18px;">'.$amount.' ₸ в месяц</span></div>');
} else if($units > 25) {
    $amount = $units * 4000;
	exit('<span style="font-weight: bold; font-size: 18px;">'.$amount.' ₸ в месяц</span></div>');
}   
?>